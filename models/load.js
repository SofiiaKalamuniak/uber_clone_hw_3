const { model, Schema } = require("mongoose");

const Load = new Schema({
  created_by: { type: Object, required: false },
  assigned_to: { type: Object, required: true },
  status: { type: String, required: true },
  state: { type: String, required: true },
  name: { type: String, required: true },
  payload: { type: Number, required: true },
  pickup_address: { type: String, required: true },
  delivery_address: { type: String, required: true },
  logs: 
   [ { message: { type: String, required: true },
      time: { type: Date, required: true }
    }
 ],
  dimensions: {
    width: { type: Number, required: true },
    length: { type: Number, required: true },
    height: { type: Number, required: true }
  },
  createdDate: { type: Date, required: true },
  driver_found: {type: Boolean, required: true }
});

module.exports = model("Load", Load);
