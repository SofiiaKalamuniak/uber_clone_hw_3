const { model, Schema} = require('mongoose')

const Truck = new Schema({
        created_by: { type: Object , required: true},
        assigned_to: { type: Object , required: true},
        status: {type: String, required: true},
        type: {type: String, required: true},
        payload: { type: Number, required: true },
        dimensions: {
                width: { type: Number, required: true },
                length: { type: Number, required: true },
                height: { type: Number, required: true }
              }
})


module.exports = model('Truck', Truck)
