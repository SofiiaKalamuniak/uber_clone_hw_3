const express = require("express");
const mongoose = require('mongoose')
const authRouter = require('./routes/auth.routes')
const config = require('config')
const app = express();
const Port = config.get('serverPort')
const corsMiddleware = require('./middleware/cors.middleware')
const morgan = require("morgan");

app.get("/", (req, res) => {
  res.send("Start");
});

app.use(
  morgan(":method :url :status :res[content-length] - :response-time ms")
);
app.use(express.urlencoded({ extended: false }));

app.use(corsMiddleware)
app.use(express.json())
app.use('/api', authRouter) 

const start = async() => {
  try {
    console.log('Try to start');
    await mongoose.connect(config.get('dbUrl'))
    app.listen(Port, () => {
      console.log(`Start at http://localhost:${Port}`);
    });
  } catch(e) {
    console.log(e)
  }
};

start()