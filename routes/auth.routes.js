const Router = require('express')
const User = require('../models/user')
const Driver = require('../models/truck')
const router = new Router()

const controller = require('./auth.controller')
const {check} = require('express-validator')
const authMiddleware = require('../middleware/auth.middleware')



router.post('/auth/register',[
    check('email','email cannot be empty').isEmail(),
    check('password',
    'password must be longer than 6 characters and shorter than 50 characters')
    .isLength({min:1, max:150})
], controller.registration)
router.post('/auth/login', controller.login)
router.get('/users/me', authMiddleware, controller.getUser)
router.delete('/users/me', authMiddleware, controller.deleteUser)
router.patch('/users/me/password', authMiddleware, controller.changePassword)



router.get('/trucks', authMiddleware, controller.getTrucks )
router.get('/trucks/:id', authMiddleware, controller.getTruck )
router.delete('/trucks/:id', authMiddleware, controller.deleteTruckById)
router.post('/trucks', authMiddleware, controller.createTruck)
router.post('/trucks/:id/assign', authMiddleware, controller.assignTruck)
router.put('/trucks/:id',authMiddleware, controller.updateTruck)


router.post('/loads', authMiddleware, controller.createLoads)
router.get('/loads', authMiddleware, controller.getLoads)
router.get('/loads/active', authMiddleware, controller.getActiveLoads)
router.patch('/loads/active/state', authMiddleware, controller.getActiveStateLoads )
router.get('/loads/:id', authMiddleware, controller.getLoad)
router.put('/loads/:id',authMiddleware, controller.updateLoad)
router.delete('/loads/:id', authMiddleware, controller.deleteLoadById)
router.post('/loads/:id/post', authMiddleware, controller.postLoad)
router.get('/loads/:id/shipping_info', authMiddleware, controller.getLoadShippingInfo)


module.exports = router