const User = require("../models/user");
const Truck = require('../models/truck');
const Load = require('../models/load');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");
const config = require("config");
const JWTSecret = config.get("jwtsecter");

const generateAccessToken = (id) => {
  const payload = {
    id
  };
  return jwt.sign(payload, JWTSecret, { expiresIn: "24h" });
};



class AuthController {
  async registration(req, res) {
    try {
      const err = validationResult(req);
      if (!err.isEmpty()) {
        console.log(err);
        return res.status(400).send({ message: "Error message isE" });
      }
      const { email, password, role } = req.body;
      const candidate = await User.findOne({ email });
      if (candidate) {
        return res.status(400).send({ message: "Error message candidate" });
      }
      const hashPassword = bcrypt.hashSync(password, 7);
      const user = new User({
        email,
        password: hashPassword,
        role,
        createdDate: new Date()
      });
      await user.save();
      return res.status(200).send({ message: "Success" });
    } catch (e) {
      console.log(e);
      res.status(400).send({ message: "Error message catch" });
    }
  }

  async login(req, res) {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(400).send({ message: "Error message" });
      }

      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).send({ message: "Error message" });
      }
      const token = generateAccessToken(user._id);
      return res.status(200).send({
        jwt_token: token
      });
    } catch (e) {
      console.log(e);
      res.status(400).send({ message: "Error message" });
    }
  }
  

  async getUser(req, res) {
    try {
      const user = await User.findById(req.user.id);
      const result = {
        user: {
          _id: user.id,
          role: user.role,
          email: user.email,
          createdDate: user.createdDate
        }
      };
     return res.status(200).send(result);
    } catch (e) {
      res.status(400).send({ message: "Error message" });
      throw e;
    }
  }

  async deleteUser(req, res) {
    try {
       await User.findByIdAndRemove(req.user.id) 
       return res.status(200).send({
          message: 'Success'
        });
    } catch (e) {
      res.status(400).send({ message: 'Error message' });
      throw e;
    }
  }

  async changePassword(req, res) {
    try {
        const user = await User.findOne({ _id: req.user.id })
        const validOldPassword = await bcrypt.compare(req.body.oldPassword, user.password);
        if (!validOldPassword) {
            res.status(400).send({ message: "Error message" })
        } else {
            const salt = await bcrypt.genSalt(7)
            const hashPassword = await bcrypt.hash(req.body.newPassword, salt)
            await User.findOneAndUpdate({ _id: req.user.id }, { password: hashPassword })
            return res.status(200).send({ message: 'Success' })
        }
    } catch (error) {
        console.error(error)
        res.status(400).send({ message: "Error message" })
    }
}

    async getTrucks(req, res) {
        try {
        const user = await User.findById(req.user.id);
        if(user.role.toUpperCase() === 'driver'.toUpperCase()){
          const truck = await Truck.find({}).select('-__v');
          const result = {
            trucks: truck
          };
         return res.status(200).send(result);
        } else {
            res.status(400).send({ message: 'User is not a driver.' });
        } 
    } catch (e) {
          res.status(400).send({ message: "Error message" });
          throw e;
        }
      }


    async createTruck(req, res) {
        try {
          console.log("req id",req.user.id)
        const user = await User.findById(req.user.id);
        if(user.role.toUpperCase() === 'driver'.toUpperCase()){
        const {type} = req.body;
      
          const truck = new Truck({
            created_by: req.user.id ,
            assigned_to : '' ,
            status: "IS",
            type: req.body.type,
            payload: 0,
            dimensions: {
              width: 0 ,
              length: 0,
              height: 0
            },
            createdDate: new Date()
          });
          if(type.toUpperCase() === 'sprinter'.toUpperCase()){ 
            truck.dimensions.width= 300
            truck.dimensions.length= 250
            truck.dimensions.height= 170
            truck.payload = 1700
          } else if(type.toUpperCase() === 'small straight'.toUpperCase()){ 
            truck.dimensions.width= 500
            truck.dimensions.length= 250
            truck.dimensions.height= 170
            truck.payload = 2500
          } else if(type.toUpperCase() === 'large straight'.toUpperCase()){ 
            truck.dimensions.width= 700
            truck.dimensions.length= 350
            truck.dimensions.height= 200
            truck.payload = 4000
          } 
          await truck.save();
         return res.status(200).send({ message: 'Success' });
        } else {
            res.status(400).send({ message: 'User is not a driver.' });
        }
        } catch (e) {
          res.status(400).send({ message: e });
          throw e;
        }
      }
    

    async assignTruck(req, res) {
        try {
            const user = await User.findById(req.user.id);
            if(user.role.toUpperCase() === 'driver'.toUpperCase()){
            const userTruckAssign = await Truck.find({ assigned_to: req.user.id })
            if (userTruckAssign.length > 0 ){
              return res.status(400).send({ message: 'User already assigned truck.' });
            }
            await Truck.findOneAndUpdate(
             { _id: req.params.id },
             { assigned_to: req.user.id }
           );
          return res.status(200).send({
             message: 'Success'
           });
        } else {
            res.status(400).send({ message: 'User is not a driver.' });
        }
         } catch (e) {
           res.status(400).send({ message: 'Error message' });
           throw e;
         }
        }
    
        async getTruck(req, res) {
            try {
            const user = await User.findById(req.user.id);
            if(user.role.toUpperCase() === 'driver'.toUpperCase()){
              const truck = await Truck.findById(req.params.id).select('-__v').select('-dimensions').select('-payload');
              return res.status(200).send({
                truck: truck
              });
            } else {
                res.status(400).send({ message: 'User is not a driver.' });
            }
            } catch (e) {
              res.status(400).send({ message: 'Error message' });
              throw e;
            }
          }

        async updateTruck(req, res) {
            try {
            const user = await User.findById(req.user.id);
            if(user.role.toUpperCase() === 'driver'.toUpperCase()){
               await Truck.findOneAndUpdate(
                { _id: req.params.id },
                { type: req.body.type }
              );
              res.status(200).send({
                message: 'Success'
              });
            } else {
                res.status(400).send({ message: 'User is not a driver.' });
            }
            } catch (e) {
              res.status(400).send({ message: 'Error message' });
              throw e;
            }
          }

          async deleteTruckById(req, res) {
            try {
                if (await Truck.findOneAndDelete({ _id: req.params.id })) {
                    console.log('Truck deleted success')
                    return res.status(200).send({ message: 'Success' })
                } else {
                    console.log('No Truck with this id')
                    return res.status(400).send({ message: "Error message" })
                }
            } catch (error) {
                console.error(error)
                res.status(400).send({ message: "Error message" })
            }
        }
  
        async getActiveLoads(req, res) {
          try {
            const user = await User.findById(req.user.id);
            if(user.role.toUpperCase() === 'driver'.toUpperCase()){
             const load = await Load.find({assigned_to : req.user.id})
             .where("status").equals("ASSIGNED")
             .select('-__v')
             .select('-driver_found')
              return res.status(200).send({load: load})
                  } else {
                    return res.status(400).send({ message: 'User is not a driver.' });
                  }
                  } catch (e) {
                    return res.status(400).send({ message: e });
                  } 
          }


          async getActiveStateLoads(req, res) {
            try {
              const user = await User.findOne({ _id: req.user.id });
              const truck = await Truck.findOne({assigned_to : req.user.id })
              const load = await Load.findOne({assigned_to : req.user.id })
              if(user.role.toUpperCase() === 'driver'.toUpperCase()){
               if (load.state === 'En route to Pick Up'){
                 load.state = 'Arrived to Pick up'
               } else if (load.state === 'Arrived to Pick up'){
                load.state = 'En route to Delivery'
              }else if (load.state === 'En route to Delivery'){
                load.state = 'Arrived to Delivery'
                load.status = 'SHIPPED'
                truck.status = 'IS'
                truck.save()
              }
                load.save()
               if(!load){
                 return res.status(400).send({ message: 'state cant be change' })
               }
                return res.status(200).send({
                  message: "Load state changed"
                }) 
              } else {
                      return res.status(400).send({ message: 'User is not driver.' });
                    }
                    } catch (e) {
                      return res.status(400).send({ message: e });
                    } 
            }

 
          
          async createLoads(req, res) {
            try {
            const user = await User.findById(req.user.id);
            if(user.role.toUpperCase() === 'shipper'.toUpperCase()){
              const load = new Load({
                created_by: req.user.id ,
                assigned_to : '' ,
                status: "NEW",
                state: ' ',
                name: req.body.name, 
                payload: req.body.payload, 
                pickup_address: req.body.pickup_address, 
                delivery_address: req.body.delivery_address, 
                logs: [
                  {
                    message: "Load assigned to driver",
                    time:  new Date()
                  }
              ],
                dimensions: {
                width: req.body.dimensions.width, 
                length: req.body.dimensions.length,
                height: req.body.dimensions.height
              },
                createdDate: new Date(),
                driver_found: false
              });
              await load.save();
              res.status(200).send({ message: 'Success' });
            } else {
                res.status(400).send({ message: 'User is not a shipper.' });
            }
            } catch (e) {
              res.status(400).send({ message: e });
              throw e;
            }
          }

        
          async getLoads(req, res) {
            try {
            
              const offset = +req.query.offset || 0
              const limit = +req.query.limit || 0
          
              const user = await User.findById(req.user.id);

            if(user.role.toUpperCase() === 'shipper'.toUpperCase()){
              const load = await Load.find({ created_by: req.user.id, status: req.query.status || { $ne: 'SHIPPER' } })
              .skip(offset)
              .limit(limit).select('-__v').select('-river_found')
              
              const result = {
                offset: offset,
                limit: limit,
                loads: load
              };
              res.status(200).send(result);
            } else {
                res.status(400).send({ message: 'User is shipper.' });
            } 
        } catch (e) {
              res.status(400).send({ message: "Error message" });
              throw e;
            }
          }


          async getLoad(req, res) {
            try {
            const user = await User.findById(req.user.id);
             if(user.role.toUpperCase() === 'shipper'.toUpperCase()){
              const load = await Load.findById(req.params.id).select('-__v').select('-driver_found');
              return res.status(200).send({
                load: load
              });
            } else {
                res.status(400).send({ message: 'User is shipper.' });
            }
            } catch (e) {
              res.status(400).send({ message: 'Error message' });
              throw e;
            }
          }

          
          async updateLoad(req, res) {
            try {
            const user = await User.findById(req.user.id);
             if(user.role.toUpperCase() === 'shipper'.toUpperCase()){
               await Load.findOneAndUpdate(
                { _id: req.params.id },
                { 
                      name: req.body.name, 
                      payload: req.body.payload, 
                      pickup_address: req.body.pickup_address, 
                      delivery_address: req.body.delivery_address, 
                      dimensions: {
                      width: req.body.dimensions.width, 
                      length: req.body.dimensions.length,
                      height: req.body.dimensions.height
                    }
                  }
              );
              res.status(200).send({
                message: 'Success'
              });
            } else {
                res.status(400).send({ message: 'User is not a shipper.' });
            }
            } catch (e) {
              res.status(400).send({ message: 'Error message' });
              throw e;
            }
          }

          async deleteLoadById(req, res) {
            try {
                if (await Load.findOneAndDelete({ _id: req.params.id })) {
                    console.log('Load deleted success')
                    return res.status(200).send({ message: 'Success' })
                } else {
                    console.log('No Load with this id')
                    return res.status(400).send({ message: "Error message" })
                }
            } catch (error) {
                console.error(error)
                res.status(400).send({ message: "Error message" })
            }
        }


        async postLoad(req, res) {
          try {
          const user = await User.findById(req.user.id);
          if(user.role.toUpperCase() === 'shipper'.toUpperCase()){
           const load = await Load.findOneAndUpdate( { _id: req.params.id },
              {
                status: "POSTED"
            })
            let availTruck = await Truck
            .findOne({})
            .where("status").equals("IS")
            .where("assigned_to").ne('')
            .where('width').gt(load.dimensions.width)
            .where('length').gt(load.dimensions.length)
            .where('height').gt(load.dimensions.height)
            .where('payload').gt(load.payload)
            .exec()
           
         
            if(!availTruck){
              await Load.findOneAndUpdate( { _id: req.params.id },
                {
                  status: "NEW",
                  logs: [
                    {
                      message: "Driver not found",
                      time:  new Date()
                    }
                ]
              })
              return res.status(200).send({ message: 'Truck is not found.' });
            }
           
            const driverTruck = await Truck.findOneAndUpdate( { _id: availTruck.id },
              {
                status: "OL"
            })
            await Load.findOneAndUpdate( { _id: req.params.id },
              {
                status: "ASSIGNED",
                state: "En route to Pick Up",
                assigned_to: driverTruck.assigned_to,
                driver_found: true,
                logs: [
                  {
                    message: "Load assigned to driver",
                    time:  new Date()
                  }
              ]
            })
            return res.status(200).send({message: "Success"})
                } else {
                  return res.status(400).send({ message: 'User is not a shipper.' });
                }
                } catch (e) {
                  return res.status(400).send({ message: e });
                }
              }


              async getLoadShippingInfo(req, res) {
                try {
                  const user = await User.findById(req.user.id);
                  if(user.role.toUpperCase() === 'shipper'.toUpperCase()){
                   const load = await Load.findOne({_id: req.params.id})
                   .select('-__v').select('-driver_found')
                   const driver = await User.findOne({_id: load.assigned_to})
                   const truck = await Truck.findOne({ assigned_to: driver._id.toString()})
                   .select('-__v').select('-driver_found')
                    return res.status(200).send({
                      load: load ,
                      truck: truck
                    })
                        } else {
                          return res.status(400).send({ message: 'User is not shipper.' });
                        }
                        } catch (e) {
                          return res.status(400).send({ message: e });
                        } 
                }

            
}

module.exports = new AuthController();
